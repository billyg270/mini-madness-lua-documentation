<h1>setVelocity(<span title="number">x</span>, <span title="number">y</span>, <span title="number">z</span>)</h1>
<br>
This can be used to make an entity go flying!
<br>
<br>
In this example we are spawning in a cow at 0, 0, 0 and then making it jump up really high
<br>
<pre>
<code>local <a href="?page=Object Types/LivingEntity">jumpingCow</a> = <a href="?page=Globals/overworld">overworld</a>:<a href="?page=Object Types/World/spawnEntity.php">spawnEntity</a>(0, 0, 0, "cow")
<a href="?page=Object Types/LivingEntity">jumpingCow</a>:setVelocity(0, 3, 0)
</code>
</pre>