<h1>overworld</h1>
<br>
The overworld is a global variable you can access within your scripts.
<br>
To see what you can do with it, see the object type, <a href="?page=Object Types/World">World</a>.