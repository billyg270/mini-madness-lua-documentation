<code>
<a href="?page=Events">events</a>.onPlayerChat(<a href="?page=Object Types/Player">player</a>, <span title="string">message</span>)
</code>
<br>
<br>
In this example if the player said hello the server will broadcast a message saying hello back, inclusing the player's name
<pre>
<code>events.onPlayerChat = function(<a href="?page=Object Types/Player">player</a>, <span title="string">message</span>)
    if <span title="string">message</span> == "hello" then
        chat:broadcast("Hey, " .. <a href="?page=Object Types/Player">player</a>:<a href="?page=Object Types/Player/getName.php">getName()</a> .. " how's it going?")
    end
end
</code>
</pre>
<br>
<div>
	You can return true, if you do not want the message to be sent.
</div>
<pre>
<code>events.onPlayerChat = function(<a href="?page=Object Types/Player">player</a>, <span title="string">message</span>)
    if <span title="string">message</span> == "Voldemort" then
        return true
    end
end
</code>
</pre>